Looking to buy or sell a home in Whitehorse, Yukon, Canada? Get in touch with Scott Sauer REALTOR® Re/Max Action Realty. I love my job and if you want immediate professional advice about Whitehorse real estate i'm happy to help.

Address: 49B Waterfront Place, Whitehorse, Yukon Y1A 6V1, Canada

Phone: 867-333-1095

Website: http://liveinwhitehorse.ca
